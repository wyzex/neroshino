from django.conf.urls.defaults import *
from django.contrib import admin

from registration.views import register
from lbforum.accountviews import profile
from django.conf.urls.static import static
from django.conf import settings

admin.autodiscover()

urlpatterns = patterns('',
#    url(r'^accounts/register/$',
#        register,
#        { 'backend':     'lbforum.backends.simple.SimpleBackend' },
#        name='registration_register'),
#    url(r'^accounts/register/', include('simpleavatar.urls')),
    url(r'^user/(?P<user_id>\d+)/$', profile, name='user_profile'),
    #(r'^accounts/avatar/', include('simpleavatar.urls')),
    (r'^accounts/', include('registration.backends.simple.urls')),
    (r'^attachments/', include('attachments.urls')),
    url(r'^captcha/', include('captcha.urls')),
    (r'^', include('lbforum.urls')),

    # Uncomment the admin/doc line below and add 'django.contrib.admindocs'
    # to INSTALLED_APPS to enable admin documentation:
    (r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    (r'^admin/', include(admin.site.urls)),
)

urlpatterns += static(settings.MEDIA_ROOT, document_root=settings.MEDIA_URL)
